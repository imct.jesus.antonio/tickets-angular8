import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

/**Componentes para ruteo */
import { HomeComponent } from './home';
import { LoginComponent } from './login';
import { RegisterComponent } from './register';
import { TicketsComponent } from './tickets';
import { TicketDetailComponent } from './ticket-detail';
import { AddTicketComponent } from './add-ticket';
import { ProfessionalBiographyComponent } from './professional-biography';

/**Guarda de rutas */
import { AuthGuard } from './_helpers'

const routes: Routes = [
  { path: '', component: HomeComponent, canActivate: [AuthGuard] },
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: 'tickets', component: TicketsComponent, canActivate: [AuthGuard], children: [
    { path: 'ticketdetail/:id', component: TicketDetailComponent, canActivate: [AuthGuard] },
    { path: 'addticket', component: AddTicketComponent, canActivate: [AuthGuard] }
  ] },
  { path: 'biography', component: ProfessionalBiographyComponent, canActivate: [AuthGuard] },

  // Alguna otra redirigue a home
  { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
