import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { AccountService } from '../_services'

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {
  // Bandera que indica si esta o no cargando el servicio
  loadingFlag: boolean;
  // Modelo que almacena el estado del formulario
  registerForm: FormGroup;
  // almacena el error que surja
  error: string;

  /**@description Constructor del modulo */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private accountService: AccountService
  ) { 
    
  }
  /**@description Inicio del modulo */
  ngOnInit() {
    this.registerForm = this.createFormGroup();
    this.loadingFlag = false;
  }

  /**@description Funcion que crea y registra 
   * los datos del formulario */
  onRegister(){
    // comprobamo el estado del formulario
    if(this.registerForm.invalid){
      return;
    }
    // Bandera de carga activa
    this.loadingFlag = true;
    let newAccount = this.registerForm.getRawValue();
    // Consoleamos
    console.log(newAccount);
    //return;
    // Hacemos la peticion
    this.accountService.createAccount(
      newAccount.email,
      newAccount.password,
      newAccount.fullname,
      newAccount.lastname,
      newAccount.secondlastname,
      newAccount.birthday,
      newAccount.genere)
      .pipe(first())
      .subscribe(data => {
        this.loadingFlag = false;
        this.router.navigate(['login']);
      }),
      error => {
        this.error = error;
        this.loadingFlag = false;
        console.error(error);
      };
    console.log(this.registerForm.value);
  }

  

  /**
   * @description coleccion de los generos disponibles
   */
  genreCollection = [
    {id:"Masculino",value:"Masculino"},
    {id:"Femenino",value:"Femenino"},
    {id:"Otro",value:"Otro"}];

  createFormGroup() {
    return new FormGroup({
      fullname: new FormControl('',[
        Validators.required,
        Validators.maxLength(20)
      ]),
      lastname: new FormControl('',[
        Validators.required,
        Validators.maxLength(20)
      ]),
      secondlastname: new FormControl('',[
        Validators.required,
        Validators.maxLength(20)
      ]),
      birthday: new FormControl('',[
        Validators.required
      ]),
      genere: new FormControl('',[
        Validators.required
      ]),
      email: new FormControl('',[
        Validators.required,
        Validators.email
      ]),
      username: new FormControl('',[
        Validators.required
      ]),
      password: new FormControl('',[
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(8)
      ]),
      confirm : new FormControl('',[
        Validators.required,
        Validators.maxLength(20),
        Validators.minLength(8)
      ])
    });
  }

  get fullname(){ return this.registerForm.get('fullname');}
  get lastname(){ return this.registerForm.get('lastname');}
  get secondlastname(){ return this.registerForm.get('secondlastname');}
  get birthday(){ return this.registerForm.get('birthday');}
  get genere(){ return this.registerForm.get('genere');}
  get email(){ return this.registerForm.get('email');}
  get username(){ return this.registerForm.get('username');}
  get password(){ return this.registerForm.get('password');}
  get confirm(){ return this.registerForm.get('confirm');}

  

}
