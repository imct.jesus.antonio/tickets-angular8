import { Component, OnInit } from '@angular/core';

import { AuthenticationService } from '../_services'

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  /**@description constructor de componente */
  constructor(private authenticationService: AuthenticationService) { 
    // Suscripcion a el estado de la conexion con el usuario
    this.authenticationService.currenUser.subscribe(x => this.currentUser = x);
  }
  /**@description evento de inicio */
  ngOnInit() {
  }

  // Usuario logueado
  currentUser: any; 

  // Collection de los skils
  skillCollection = [
    {
      title: "Dotnet",
      img:"../assets/cSharp.png",
      width:"30",
      height:"30"
    },
    {
      title: "SqlServer",
      img:"../assets/sqlserver.png",
      width:"38",
      height:"38"
    },
    {
      title: "MySql",
      img:"../assets/mysql.png",
      width:"38",
      height:"38"
    },
    {
      title: "EntityFramework",
      img:"../assets/EfCore.png",
      width:"38",
      height:"38"
    },
    {
      title: "Html",
      img:"../assets/html5.png",
      width:"38",
      height:"38"
    },
    {
      title: "Css",
      img:"../assets/css.png",
      width:"38",
      height:"38"
    },
    {
      title: "Javascript",
      img:"../assets/javascript.png",
      width:"38",
      height:"38"
    },
    {
      title: "Angular",
      img:"../assets/angular.png",
      width:"38",
      height:"38"
    },
    {
      title: "Vue",
      img:"../assets/vuejs.png",
      width:"38",
      height:"38"
    },
    {
      title: "Git",
      img:"../assets/git.png",
      width:"38",
      height:"38"
    }
  ];

}
