import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../_services'

@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivate {
  /**@description constructor del helper */
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ){}

  /**@description Metodo que define si una ruta puede o no actiavarse */
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
    // Deifnimos la constante que almacena al usuario
    const currenUser = this.authenticationService.currentUserValue;
    // Verificamos si esta logueado
    if(currenUser){
      return true;
    }

    // Si no esta logeado lo mandamos a la pagina de inicio
    this.router.navigate(['/login'],{queryParams:{ returnUrl: state.url}});
    return false;
  }
  
}
