import { Component } from '@angular/core';
import { Router } from '@angular/router'

import { AuthenticationService } from './_services'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  // title de la pagina
  title = 'AxosTest';
  //Collection de menus para el usuario
  menuCollection = [
    {
      name: 'Inicio',
      icon: 'fa fa-home',
      ref: '/',
      hasSubmenus: false
    },
    {
      name: 'Notificaciones',
      icon: 'fa fa-bell',
      ref: '/',
      hasSubmenus: false
    },
    {
      name: 'Tickets',
      icon: 'fa fa-ticket',
      ref: '/tickets',
      hasSubmenus: false
    },
    {
      name: 'Proveedores',
      icon: 'fa fa-user',
      ref: '/providers',
      hasSubmenus: false
    },
    {
      name: 'Usuarios',
      icon: 'fa fa-user',
      ref: '/users',
      hasSubmenus: false
    },
    {
      name: 'Proveedores',
      icon: 'fa fa-truck',
      ref: '#TipoProveedor',
      submenuId: 'TipoProveedor',
      hasSubmenus: true,
      submenus: [
        {
          name: 'Nacional',
          icon: 'fa fa-home',
          ref: '#',
        },
        {
          name: 'Internacional',
          icon: 'fa fa-home',
          ref: '#',
        }
      ]
    },
    {
      name: 'Acerca de',
      icon: 'fa fa-bolt',
      ref: '/about',
      hasSubmenus: false
    },
    {
      name: 'Biografia Profesional',
      icon: 'fa fa-bolt',
      ref: '/biography',
      hasSubmenus: false
    }];
  // Usuario logueado
  currentUser: any; 
  
  /**@description Constructor del componente copntenedor */
  constructor(
    private router: Router,
    private authenticationService: AuthenticationService
  ){
    // Suscripcion a el estado de la conexion con el usuario
    this.authenticationService.currenUser.subscribe(x => this.currentUser = x);
  }

  /**@description Cierra la sesion del usuario */
  onLogout(){
    console.log("Cerrando session");
    this.authenticationService.logout().subscribe((result)=>{
      if(result.State){
        this.router.navigate(['/login']);
      }
    });
  }

  /**
   * @description Muestra o oculta la barra de menus lateral
   * @param $event evento que indica lo que pasa al clickar
   */
  onSidebarToggled($event){
    console.log('Toggled¡¡¡',$event)
    document.getElementById("sidebar")
    .classList.toggle("toggled");
  }
}
