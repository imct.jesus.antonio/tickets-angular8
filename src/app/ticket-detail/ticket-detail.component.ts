import { Component, OnInit, Renderer2, ViewChild, ElementRef  } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Location } from '@angular/common';

import { TicketsService } from '../_services'
import { first } from 'rxjs/operators';


@Component({
  selector: 'app-ticket-detail',
  templateUrl: './ticket-detail.component.html',
  styleUrls: ['./ticket-detail.component.css']
})
export class TicketDetailComponent implements OnInit {

  /**
   * @description Contructor del componente
   * @param actRoute Modulo de ruteo
   */
  constructor(
    private actRoute: ActivatedRoute,
    private location: Location,
    private renderer: Renderer2,
    private ticketService: TicketsService,
    private router: Router
    ) { 
      // Mandar a buscar el servicio que da la info del ticket
      // Meter al registro en el objeto del ticket
    this.ticketForm = this.createFormGroup();
  }

  ticketLoad : {
    ReceiptId,
    Currency,
    Amount,
    Comments,
    ProviderId
  };

  /** @description Ciclo de inicio de vida */
  ngOnInit() {
    this.ticketId = this.actRoute.snapshot.paramMap.get('id');
    if(this.ticketId === null){
      this.router.navigate(['tickets']);
    }
    this.ticketService.getTicket(this.ticketId)
    .pipe(first())
    .subscribe(
      data => {
        console.log(data);
        this.ticketLoad.ReceiptId = data.Unique.ReceiptId;
        this.ticketLoad.Currency = data.Unique.Currency;
        this.ticketLoad.Amount = data.Unique.Amount;
        this.ticketLoad.Comments = data.Unique.Comments;
        this.ticketLoad.ProviderId = data.Unique.ProviderId;
      },
      error => {
        console.log("Error al cargar el regisro");
        this.router.navigate(['tickets']);
      }
    );

  }

  /**
   * @description Crea el formulario en codigo
   */
  createFormGroup() {
    return new FormGroup({
      Id: new FormControl(this.ticketLoad.ReceiptId),
      currency: new FormControl(this.ticketLoad.Currency),
      amount: new FormControl(this.ticketLoad.Amount),
      comments: new FormControl(this.ticketLoad.Comments),
      provider: new FormControl(this.ticketLoad.ProviderId)
    });
  }

  /**
   * @description Cierra el modal y retorna a la ruta anterior
   */
  onClose(){
    this.router.navigate(['tickets']);
  }

  /**
   * @description Activa la edicion en el modal y habilita el guardado
   */
  onEdit(){

  }

  ticketReceipt : any;

  /*ticketReceipt = {
    ReceiptId: 1,
    Currency: "MXN",
    Amount: 2000,
    Comments: "Compra realizada con exito",
    ProviderId: 1,
    Name: "Netflix"
  };*/
  ticketForm : FormGroup;
  ticketId : String;

  providerCollection = [
    {id:1,value:"Netflix"},
    {id:1,value:"Disneyplus"},
    {id:1,value:"ClaroVideo"},
    {id:1,value:"CineCalidad"}
  ];
}
