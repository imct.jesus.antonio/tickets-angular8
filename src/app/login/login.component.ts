import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'
import { Router, ActivatedRoute } from '@angular/router';
import { first } from 'rxjs/operators';

import { AuthenticationService } from '../_services'

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  /**@description Validador del formulario de inicio de sesion */
  loginForm: FormGroup;
  /**@description Bandera que permite activar los estilos de carga */
  loadingFlag = false;
  submitted = false;
  returnUrl: string;
  error: string;
  /**
   * @description Construccion del componente
   */
  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private authenticationService: AuthenticationService
    ) {
      // Si hay un usuario
      if(this.authenticationService.currentUserValue){
        // Redirigimos al home
        this.router.navigate(['/']);
      } 
  }

  /**@description Inicio de vida del componente */
  ngOnInit() {
    // Mandamos a crear el formulario
    this.loginForm = this.createFormGroup();
    // Recuperamos la url de origen y la seteamos
    this.returnUrl = this.route.snapshot.queryParams['returnUrl'] || '/';
  }

  /**
   * @description Crea el grupo de controles 
   * para el inicio de session
   */
  createFormGroup(){
    return new FormGroup({
      username : new FormControl('',[
        Validators.required,
        Validators.email
      ]),
      password: new FormControl('',[
        Validators.required,
        Validators.maxLength(16),
        Validators.minLength(8)
      ])
    });
  }

  /**
   * Evento que se lanza al presionar el boton de
   * iniciar sesion
   */
  onsaveForm(){
    this.submitted = true;
    // Nos detenemos a verificar la valides de formulario
    if(this.loginForm.invalid){
      return;
    }

    // Bandera de carga activa
    this.loadingFlag = true;
    // Hacemos la peticion
    // Obtenemos los datos del formulario
    let newUser = this.loginForm.getRawValue();
    console.log(newUser);
    this.authenticationService.login(newUser.username,newUser.password)
    .pipe(first())
    .subscribe(
      data => {
        this.router.navigate([this.returnUrl]);
        console.info("Navegando a la siguiente pagina");
      },
      error => {
        this.error = error;
        this.loadingFlag = false;
        console.error("Error al iniciar sesion");
      }
    );
  }

  get username(){ return this.loginForm.get('username');}
  get password(){ return this.loginForm.get('password');}


  

  

  

  

}
