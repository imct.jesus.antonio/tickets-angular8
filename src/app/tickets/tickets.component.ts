import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms'

import { TicketsService } from '../_services'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-tickets',
  templateUrl: './tickets.component.html',
  styleUrls: ['./tickets.component.css']
})
export class TicketsComponent implements OnInit {
  /**@description Constructor de modulo */
  constructor(private ticketService: TicketsService) {
    // Validacion del formulario 
    this.searchForm = this.createFormGroup();
  }
  // Bandera que idnica si esta cargando la busqueda
  loadingFlag : boolean;
  // Modelo del formulario
  searchForm: FormGroup;

  /**@description Ciclo de vida inicial */
  ngOnInit() {
    // Carga,
    this.ticketService.getFilteredTickets(0,'')
    .pipe(first())
    .subscribe(
      data => {
        console.log(data);
        this.ticketsCollection = data.Unique;
      },
      error => {
        console.log("Error al cargar registros");
        this.ticketsCollection = null;
      }
    );
    this.loadingFlag = false;
  }

  

  providerCollection = [
    {id:1,value:"Netflix"},
    {id:1,value:"Disneyplus"},
    {id:1,value:"ClaroVideo"},
    {id:1,value:"CineCalidad"}
  ];

  columnCollection = [
    {name:"ID"},
    {name:"Fecha"},
    {name:"Tipo Moneda"},
    {name:"Monto total"},
    {name:"Proveedor"},
    {name:"Comentarios"},
    {name:"Acciones"}
  ];
  ticketsCollection = [
    {
      id:1,
      currency: "MXN",
      amount:2000,
      comments: "Compra realizada con exito",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:2,
      currency: "USD",
      amount:2000,
      comments: "Cntratacion de servicios",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:3,
      currency: "CAD",
      amount:2000,
      comments: "Compra de servicios ",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:4,
      currency: "JPY",
      amount:2000,
      comments: "Compra de servicios de tecnologia",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:5,
      currency: "EUR",
      amount:2000,
      comments: "Compra a empresa de la union europea",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:6,
      currency: "MXN",
      amount:2000,
      comments: "Compra realizada con exito",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:7,
      currency: "USD",
      amount:2000,
      comments: "Cntratacion de servicios",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:8,
      currency: "CAD",
      amount:2000,
      comments: "Compra de servicios ",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:9,
      currency: "JPY",
      amount:2000,
      comments: "Compra de servicios de tecnologia",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    },
    {
      id:10,
      currency: "EUR",
      amount:2000,
      comments: "Compra a empresa de la union europea",
      provider: "Netflix",
      ticketdate: "02/03/2020"
    }
  ];

  /**@description Crea el formulario en clase */
  createFormGroup(){
    return new FormGroup({
      provider : new FormControl('',null),
      currency: new FormControl('',null)
    });
  };

  /**Realiza la busqeuda de los tickets  filtrados */
  onSearchTikets(){
    this.loadingFlag = true;
    // Verificamos que el formulario sea valido
    if(this.searchForm.invalid){
      this.loadingFlag = false;
      console.log(this.searchForm.getRawValue());
      return;
    }
    // Activamos la bandera de busqeuda
   
    // si es valido obtenemos los valores
    let newFilter = this.searchForm.getRawValue();
    newFilter.provider = newFilter.provider == "" ? 0 : newFilter.provider;
    console.log(newFilter);
    // Verificamos si los datos son validos
    this.ticketService.getFilteredTickets(newFilter.provider,newFilter.currency)
    .pipe(first())
    .subscribe(
      data => {
        console.log("Datos recuperados");
        this.ticketsCollection = data.Unique;
        this.loadingFlag = false;
        return;
      },
      error => {
        console.log("Error al recuperar datos");
        this.ticketsCollection = null;
        this.loadingFlag = false;
        return;
      }
    );
  }

  /**@description Funcion encargada de eliminar un registro */
  onDeleteTicket(ticketId : number){
    console.log("Eliminado tikect");
    this.ticketService.deleteTicket(ticketId).subscribe((result) =>{
      if(result.State){
        this.ticketService.getFilteredTickets(0,'')
          .pipe(first())
          .subscribe(
            data => {
              console.log(data);
              this.ticketsCollection = data.Unique;
            },
            error => {
              console.log("Error al cargar registros");
              this.ticketsCollection = null;
            }
          );
      }
    });
    console.log(ticketId);
  }

  get provider(){ return this.searchForm.get('provider'); }
  get currency(){ return this.searchForm.get('currency'); }


}
