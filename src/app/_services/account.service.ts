import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AccountService {
  private baseUrl = environment.baseUrl;

  constructor(private http: HttpClient) { }

  createAccount(
    username, 
    password,
    name,
    lastname,
    secondlastname,
    birthday,
    gender){
      // Creamos el objeto
      let newAccount = {
        username,
        password,
        name,
        lastname,
        secondlastname,
        birthday,
        gender
      }
      console.info("Iniciando la peticion");
      console.info(this.baseUrl);
      // Realizamos la peticion
      return this.http.post<any>(`${this.baseUrl}/Accounts`,newAccount)
      .pipe(map(result => {
        console.info(result);
        return result;
      }));
  }
}
