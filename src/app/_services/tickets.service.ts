import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment';
import { MapOperator } from 'rxjs/internal/operators/map';
import { resource } from 'selenium-webdriver/http';

@Injectable({
  providedIn: 'root'
})
export class TicketsService {
  // Url para las peticiones
  private baseUrl = environment.baseUrl;
  /**@description Cpnstructor para servicio */
  constructor(private http: HttpClient) { }

  /**@description Peticion que crea un ticket de proveedor */
  createTicket(providerID,amount,currency,comments){
    // Objeto de nuevo ticket
    let newTicket = {
      providerID,
      amount,
      currency,
      comments
    };
    console.info(newTicket);
    // Realizamos la peticion
    return this.http.post<any>(`${this.baseUrl}/Receipts`,newTicket)
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  }

  /**@description Obtiene tickets filtrados */
  getFilteredTickets(providerId: number,currency: string){
    return this.http.get<any>(`${this.baseUrl}/Receipts?providerId=${providerId}&currency=${currency}`)
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  }

  /**@description Elimina un ticket con el id especificado */
  deleteTicket(id: number){
    return this.http.delete<any>(`${this.baseUrl}/Receipts?id=${id}`)
    .pipe(map(result => {
      console.info(result);
      return result;
    }));
  }

  getTicket(id: String){
    return this.http.get<any>(`${this.baseUrl}/Receipts?id=${id}`)
    .pipe(map(result => {
      console.log(result);
      return result;
    }));
  }
}
