import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http'
import { BehaviorSubject, Observable } from 'rxjs'
import { map } from 'rxjs/operators'
import { environment } from '../../environments/environment'; 

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {
  private baseUrl = environment.baseUrl;
  private currentUserSubject: BehaviorSubject<any>;
  public currenUser: Observable<any>;

  constructor(private http: HttpClient) { 
    this.currentUserSubject = new BehaviorSubject<any>(JSON.parse(localStorage.getItem('currentUser')));
    this.currenUser = this.currentUserSubject.asObservable();
  }

  public get currentUserValue(){
    return this.currentUserSubject.value;
  }

  login(username, password){
    let newsession = {
      origin: "AxosUser",
      username: username,
      password: password
    };
    return this.http.post<any>(`${this.baseUrl}/Sessions`,newsession)
    .pipe(map(result => {
      // Almacenar los detalles y el token en local
      localStorage.setItem('currentUser',JSON.stringify(result.unique));
      this.currentUserSubject.next(result.unique);
      console.info(result.unique);
      return result.Unique;
    }));
  }

  logout(){
    let user = JSON.parse(localStorage.getItem('currentUser'));
    localStorage.removeItem('currentUser');
    console.info(user.SessionId);
    console.log(`${this.baseUrl}/Sessions?sessionId=${user.SessionId}`);
    return this.http.delete<any>(`${this.baseUrl}/Sessions?sessionId=${user.SessionId}`)
    .pipe(map(result => {
      console.info(result);
      this.currentUserSubject.next(null);
      return result;
    }));
  }
}
