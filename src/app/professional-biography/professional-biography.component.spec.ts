import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfessionalBiographyComponent } from './professional-biography.component';

describe('ProfessionalBiographyComponent', () => {
  let component: ProfessionalBiographyComponent;
  let fixture: ComponentFixture<ProfessionalBiographyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProfessionalBiographyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfessionalBiographyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
