import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { Location } from '@angular/common';
import { FormGroup, FormControl, Validators } from '@angular/forms'

import { TicketsService } from '../_services'
import { first } from 'rxjs/operators';

@Component({
  selector: 'app-add-ticket',
  templateUrl: './add-ticket.component.html',
  styleUrls: ['./add-ticket.component.css']
})
export class AddTicketComponent implements OnInit {
  // Validador de formulario
  ticketForm : FormGroup;
  // Id de ticket 
  ticketId : String;
  // Error
  error : string;

  /**@description Constructor de componente */
  constructor(
    private actRoute: ActivatedRoute,
    private location: Location,
    private router: Router,
    private ticketService: TicketsService
  ) { 
    this.ticketForm = this.createFormGroup();
  }

  /**@description Ciclo de inicio de componente */
  ngOnInit() {
  }

  createFormGroup() {
    return new FormGroup({
      receiptId: new FormControl(''),
      currency: new FormControl(''),
      amount: new FormControl(''),
      comments: new FormControl(''),
      provider: new FormControl('')
    });
  }

   /**
   * @description Cierra el modal y retorna a la ruta anterior
   */
  onClose(){
    this.location.back();
  }

  /**@description Funcion que se ejecuta cuando
   * se manda a guardar el ticket
   */
  onSave(){
    // Verificamos la valides
    if(this.ticketForm.invalid){
      return;
    }

    let newTicket = this.ticketForm.getRawValue();
    console.log(newTicket);
    // Ejecutamos la adicion
    this.ticketService.createTicket(newTicket.provider,newTicket.amount,
      newTicket.currency,newTicket.comments)
      .pipe(first())
      .subscribe(
        data => {
          this.router.navigate(['tickets']);
          console.info("Ticket agregado correctamente");
        },
        error => {
          this.error = error;
          console.error("Error al agregar el ticket");
        }
      )

    // Bandera de carga actica

  }

  

  providerCollection = [
    {id:1,value:"Netflix"},
    {id:1,value:"Disneyplus"},
    {id:1,value:"ClaroVideo"},
    {id:1,value:"CineCalidad"}
  ];

}
